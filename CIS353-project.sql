SPOOL project.out
SET ECHO ON
/*
CIS 353 - Database Design Project
Justin Good
Zach Cornell
Ken Aernouts
Rodney Fulk
*/
-- Must drop tables in proper order or they wont drop.. Aka foreign keys depend on them
DROP TABLE VIDEOTRANS;
DROP TABLE TRANSACTIONS;
DROP TABLE VIDEOS;
DROP TABLE STORESECTION;
DROP TABLE STOREDIN;
DROP TABLE STAFFPOSITIONS;
DROP TABLE POSITIONS;
DROP TABLE STAFF;
DROP TABLE CUSTOMERS;
DROP TABLE ACCOUNTS;
DROP TABLE ZIPCODE;

CREATE TABLE ZIPCODE
(
Zip         	CHAR(5) 	NOT NULL,
City        	VARCHAR(30) 	NOT NULL,
State		CHAR(2) 	NOT NULL,     
CONSTRAINT zIC1 PRIMARY KEY (Zip)
);

CREATE TABLE ACCOUNTS
(
A_ID		INTEGER,
Address 	VARCHAR(155)	NOT NULL,
Zip		CHAR(5)		NOT NULL, 
CONSTRAINT aIC1 PRIMARY KEY (A_ID),
FOREIGN KEY (Zip) REFERENCES ZIPCODE(Zip)
);

CREATE TABLE STOREDIN
(
SI_ID           INTEGER,
Shelf		VARCHAR(5),
CONSTRAINT siIC1 PRIMARY KEY(SI_ID)
);

CREATE TABLE STORESECTION
(
Section_Name	CHAR(25),
Video_Capacity	INTEGER		NOT NULL,	-- how many videos can be in each section
S_ID		INTEGER,
FOREIGN KEY (S_ID) REFERENCES STOREDIN(SI_ID),
CONSTRAINT ssIC1 PRIMARY KEY(Section_Name),
CONSTRAINT ssIC2 CHECK ( section_name IN ( 'New Release', 'Classics', 'Kids', 'Action', 'Romance', 'Comedy', 'Western'))
);

CREATE TABLE VIDEOS
(
V_ID	 	INTEGER,
Title 		VARCHAR(50)	NOT NULL, 	-- Made it 50 just in case //ken
Release_Date 	DATE 		NOT NULL,	-- Listed as "Release Date" on ER, was AcqDate //ken
Condition 	CHAR(3), 			-- NEW|DEC|BAD	
Rating          CHAR(2),    			-- G|PG|13|R|NR
Genre           CHAR(15) 	NOT NULL,   	-- Western, Action, Comedy, Kids, Romance
Num_Copies      INTEGER,
Store_Section   CHAR(25) 	NOT NULL,  -- changed for the correlated subquery
FOREIGN KEY (Store_Section) REFERENCES STORESECTION(Section_Name),
CONSTRAINT vIC1 PRIMARY KEY (V_ID),
CONSTRAINT vIC2 CHECK ( condition IN ( 'New', 'Dec', 'Bad' ) ),
CONSTRAINT vIC3 CHECK ( rating IN ( 'G', 'PG', '13', 'R', 'NR' ) ),
CONSTRAINT vIC4 CHECK ( Num_Copies > 0 ),
CONSTRAINT vIC5 CHECK ( Genre IN ('Western', 'Action', 'Comedy', 'Kids', 'Romance') )
);

CREATE TABLE STAFF
(
S_ID		INTEGER,
Phone		CHAR(10),				
First_Name	VARCHAR(20)	NOT NULL,		
Last_Name	VARCHAR(20)	NOT NULL,		
Email		VARCHAR(30),
Address		VARCHAR(50) 	NOT NULL,
Zip         	CHAR(5) 	NOT NULL,				
FOREIGN KEY (Zip) REFERENCES ZIPCODE(Zip),
CONSTRAINT stIC1 PRIMARY KEY(S_ID),
CONSTRAINT stIC2 CHECK ( REGEXP_LIKE(Phone, '^[[:digit:]]{10}$'))
);

CREATE TABLE POSITIONS
(
P_ID		INTEGER,
Positions	CHAR(20) 	NOT NULL,
CONSTRAINT pIC1 PRIMARY KEY (P_ID),
CONSTRAINT pIC2 CHECK ( positions IN ( 'Manager', 'Assistant Manager', 'Staff'  ))
);

CREATE TABLE STAFFPOSITIONS
(
S_ID		INTEGER,
P_ID		INTEGER,
FOREIGN KEY (S_ID) REFERENCES STAFF(S_ID),
FOREIGN KEY (P_ID) REFERENCES POSITIONS(P_ID)
);

CREATE TABLE CUSTOMERS
(
A_ID		INTEGER	,		-- From the Account entity table
DOB		DATE,
F_Name		VARCHAR(20) NOT NULL,
L_Name		VARCHAR(20) NOT NULL,
Date_Joined	DATE,					
Phone		CHAR(10),
Email		CHAR(40),
FOREIGN KEY (A_ID) REFERENCES ACCOUNTS (A_ID),
CONSTRAINT cIC1 PRIMARY KEY (A_ID, F_Name, L_Name),
CONSTRAINT cIC2 CHECK ( REGEXP_LIKE(Phone, '^[[:digit:]]{10}$'))
);

CREATE TABLE TRANSACTIONS
(
Trans_Num 	INTEGER,
Due_Date 	DATE, 		-- Used to calculate days to due or if over due
Trans_Date 	DATE,		-- Date of transaction (Start date redundant and not needed)
Staff_OUT   	INTEGER,    	-- Staff member that logged checkout transaction
Ret_Date 	DATE,        	-- Date actually returned
Staff_IN    	INTEGER,     	-- staff member that logged checkin 
A_ID		INTEGER,    
F_Name      	VARCHAR(20),
L_Name      	VARCHAR(20),
FOREIGN KEY (Staff_IN) REFERENCES STAFF(S_ID),
FOREIGN KEY (Staff_OUT) REFERENCES STAFF(S_ID),
FOREIGN KEY (A_ID, F_Name, L_Name) REFERENCES CUSTOMERS(A_ID, F_Name, L_Name),
CONSTRAINT tIC1 PRIMARY KEY (Trans_num),
CONSTRAINT tIC2 CHECK (((Ret_Date IS NULL) AND (Staff_IN IS NULL)) OR 
					((Ret_Date IS NOT NULL) AND (Staff_IN IS NOT NULL)))
-- TODO check age of person against rating available
);

CREATE TABLE VIDEOTRANS
(
TNUM		INTEGER,
V_ID		INTEGER,
FOREIGN KEY (V_ID) REFERENCES VIDEOS(V_ID),
FOREIGN KEY (TNUM) REFERENCES TRANSACTIONS(Trans_Num)
);

--
SET FEEDBACK OFF

-- The INSERT statements that populate the tables>
   -- important: keep the number of rows in each table small 
-- Zipcode table   
INSERT INTO ZIPCODE VALUES(49442, 'Muskegon', 'MI');
INSERT INTO ZIPCODE VALUES(49401, 'Allendale', 'MI');
INSERT INTO ZIPCODE VALUES(49418, 'Grandville', 'MI');
INSERT INTO ZIPCODE VALUES(49505, 'Grand Rapids' , 'MI'); 
--- Accounts table  
INSERT INTO ACCOUNTS VALUES(1, '123 Sesame Street', 49401);
INSERT INTO ACCOUNTS VALUES(2, '1313 Mockingbird Lane', 49401);
INSERT INTO ACCOUNTS VALUES(3, '345 Sesame Street',49418);
INSERT INTO ACCOUNTS VALUES(4, '203 Market SW',49505);
INSERT INTO ACCOUNTS VALUES(5, '1300 Sandish',49505);
INSERT INTO ACCOUNTS VALUES(6, '420 Park',49442);
INSERT INTO ACCOUNTS VALUES(7, '1392 Main',49442);
INSERT INTO ACCOUNTS VALUES(8, '666 Campus Drive',49401);
--Storedin table
INSERT INTO STOREDIN VALUES(1,'AA');
INSERT INTO STOREDIN VALUES(2,'AB');
INSERT INTO STOREDIN VALUES(3,'BA');
INSERT INTO STOREDIN VALUES(4,'BB');
INSERT INTO STOREDIN VALUES(5,'CA');
INSERT INTO STOREDIN VALUES(6,'CB');
INSERT INTO STOREDIN VALUES(7,'DA');
-- Store Section Table
INSERT INTO STORESECTION VALUES ('New Release', 50, 1);
INSERT INTO STORESECTION VALUES ('Classics', 100, 2);
INSERT INTO STORESECTION VALUES ('Kids', 150, 3);
INSERT INTO STORESECTION VALUES ('Action', 200, 4);
INSERT INTO STORESECTION VALUES ('Romance', 190, 5);
INSERT INTO STORESECTION VALUES ('Comedy', 150, 6);
INSERT INTO STORESECTION VALUES ('Western', 15, 7);
-- Videos table
INSERT INTO VIDEOS VALUES (1, 'Gone with the Wand', to_date('19960725','YYYYMMDD'), 'New', 'NR', 'Western', 1, 'Western');
INSERT INTO VIDEOS VALUES (2, '420', to_date('20060128','YYYYMMDD'), 'New', 'PG', 'Action', 2, 'Action');
INSERT INTO VIDEOS VALUES (3, '1001 Ways to Cry', to_date('19860623','YYYYMMDD'), 'Bad', 'PG', 'Romance', 3, 'New Release');
INSERT INTO VIDEOS VALUES (4, 'Gone In 34 Seconds', to_date('20101225','YYYYMMDD'), 'Dec', '13', 'Action', 4, 'Action');
INSERT INTO VIDEOS VALUES (5, 'The Big The Bad and The Beautiful', to_date('19990101','YYYYMMDD'), 'New', 'PG', 'Comedy', 5, 'Comedy');
INSERT INTO VIDEOS VALUES (6, 'To Kill a Mocking Dog', to_date('20120120','YYYYMMDD'), 'Bad', 'R', 'Romance', 6, 'Romance');
INSERT INTO VIDEOS VALUES (7, 'Little Red Riding Wolf', to_date('20011019','YYYYMMDD'), 'New', '13', 'Kids', 5, 'Kids');
INSERT INTO VIDEOS VALUES (8, 'My Little Elephant', to_date('20110315','YYYYMMDD'), 'Dec', 'G', 'Kids', 2, 'New Release');
INSERT INTO VIDEOS VALUES (9, 'Bald Potter', to_date('20140129','YYYYMMDD'), 'New', 'PG', 'Western', 2, 'Western');
INSERT INTO VIDEOS VALUES (10, 'Purple Dawn', to_date('20100912','YYYYMMDD'), 'Dec', 'G', 'Action', 2, 'Action');
INSERT INTO VIDEOS VALUES (11, 'George of the City', to_date('20101212','YYYYMMDD'), 'New', 'R', 'Comedy', 10, 'Comedy');
INSERT INTO VIDEOS VALUES (12, 'Star Spots', to_date('20121215','YYYYMMDD'), 'New', '13', 'Comedy', 3, 'New Release');
INSERT INTO VIDEOS VALUES (13, 'Stuper Man', to_date('20091012','YYYYMMDD'), 'Dec', 'NR', 'Action', 2, 'Action');
INSERT INTO VIDEOS VALUES (14, 'Specific Rim', to_date('20130813','YYYYMMDD'), 'New', 'R', 'Comedy', 3, 'New Release');
INSERT INTO VIDEOS VALUES (15, 'Dirty Harriet', to_date('19671228','YYYYMMDD'), 'Bad', 'G', 'Romance', 2, 'Romance');
INSERT INTO VIDEOS VALUES (16, 'Romeo and Roger', to_date('19931210','YYYYMMDD'), 'New', '13', 'Western', 3, 'Western');
INSERT INTO VIDEOS VALUES (17, 'Up Up and POP', to_date('19960315','YYYYMMDD'), 'New', 'G', 'Western', 2, 'New Release');
INSERT INTO VIDEOS VALUES (18, 'Wizard of Sauce', to_date('19710904','YYYYMMDD'), 'Dec', 'R', 'Kids', 2, 'Kids');
INSERT INTO VIDEOS VALUES (19, 'Frosty The Snow Cone', to_date('19911201','YYYYMMDD'), 'New', 'G', 'Kids', 3, 'Kids');
-- Positions Table
INSERT INTO POSITIONS VALUES (1, 'Staff');
INSERT INTO POSITIONS VALUES (2, 'Assistant Manager');
INSERT INTO POSITIONS VALUES (3, 'Manager');
-- Staff Table
INSERT INTO STAFF VALUES (1, '1234567890', 'Sarah', 'Smith', 'here@there.com', '1734 Star ST', '49505');
INSERT INTO STAFF VALUES (2, '5554567890', 'George', 'Monkey', 'nowhere@here.com', '1311 Mockingbird LN', '49418');
INSERT INTO STAFF VALUES (3, '5554563400', 'Dale', 'Hall', 'lilebopeep@gvsu.edu', '1734 Star ST', '49442');
INSERT INTO STAFF VALUES (4, '5551112222', 'Sue', 'Saraha', 'sue@boynames.com', '1734 Star ST', '49442');
INSERT INTO STAFF VALUES (5, '5551234890', 'Tom', 'Hill', 'here@kingof.com', '1734 Star ST', '49505');
INSERT INTO STAFF VALUES (6, '5550001234', 'Dorkia', 'Handsmith', 'travler@world.com', '1734 Star ST', '49401');
INSERT INTO STAFF VALUES (7, '5555551313', 'Tapica', 'Realman', 'lilfish@bigpond.com', '1734 Star ST', '49401');
INSERT INTO STAFF VALUES (8, '5551237654', 'Horge', 'Rodrigous', 'downsouth@mytaco.com', '1734 Star ST', '49401');
-- Staff postion table
INSERT INTO STAFFPOSITIONS VALUES(1, 1);
INSERT INTO STAFFPOSITIONS VALUES(2, 1);
INSERT INTO STAFFPOSITIONS VALUES(3, 1);
INSERT INTO STAFFPOSITIONS VALUES(4, 1);
INSERT INTO STAFFPOSITIONS VALUES(5, 1);
INSERT INTO STAFFPOSITIONS VALUES(6, 1);
INSERT INTO STAFFPOSITIONS VALUES(6, 2);
INSERT INTO STAFFPOSITIONS VALUES(7, 1);
INSERT INTO STAFFPOSITIONS VALUES(7, 2);
INSERT INTO STAFFPOSITIONS VALUES(8, 1);
INSERT INTO STAFFPOSITIONS VALUES(8, 3);

-- Customers Table
INSERT INTO CUSTOMERS VALUES (1, to_date('19671228','YYYYMMDD'), 'Sue', 'Rabinski', to_date('20131201','YYYYMMDD'), '5551234667', 'home1@home.com');
INSERT INTO CUSTOMERS VALUES (1, to_date('19670727','YYYYMMDD'), 'Tom', 'Rabinski', to_date('20131210','YYYYMMDD'), '5555678123', 'home2@home.com');
INSERT INTO CUSTOMERS VALUES (1, to_date('19751201','YYYYMMDD'), 'Dick', 'Rabinski', to_date('10120212','YYYYMMDD'), '5556667777', 'home3@home.com');
INSERT INTO CUSTOMERS VALUES (2, to_date('19931215','YYYYMMDD'), 'Sally', 'Smith', to_date('19991225','YYYYMMDD'), '5558889999', 'there@somewhere.com');
INSERT INTO CUSTOMERS VALUES (2, to_date('19960330','YYYYMMDD'), 'Darin', 'Smith', to_date('20121212','YYYYMMDD'), '5550102222', 'dont@gothere.com');
INSERT INTO CUSTOMERS VALUES (3, to_date('19890202','YYYYMMDD'), 'Dale', 'Eatheart', to_date('20111111','YYYYMMDD'), '5554443333', 'dont2@gothere.com');
INSERT INTO CUSTOMERS VALUES (4, to_date('19600428','YYYYMMDD'), 'Mary', 'Jarkowski', to_date('20101010','YYYYMMDD'), '5554435566', 'me@msn.com');
INSERT INTO CUSTOMERS VALUES (5, to_date('19801015','YYYYMMDD'), 'Beth', 'Badbreath', to_date('20090909','YYYYMMDD'), '5551239875', 'diced@yahoo.com');
INSERT INTO CUSTOMERS VALUES (6, to_date('19810813','YYYYMMDD'), 'Tina', 'Babe', to_date('20080808','YYYYMMDD'), '5554446666', 'sliced@hotmail.com');
INSERT INTO CUSTOMERS VALUES (7, to_date('19931203','YYYYMMDD'), 'Doug', 'Seing', to_date('20070707','YYYYMMDD'), '5553334567', 'stinky@gmail.com');
INSERT INTO CUSTOMERS VALUES (8, to_date('19960507','YYYYMMDD'), 'Tim', 'Weski', to_date('20060606','YYYYMMDD'), '5555670987', 'sink@hmail.com');
INSERT INTO CUSTOMERS VALUES (8, to_date('19910619','YYYYMMDD'), 'Dave', 'Stimey', to_date('20050505','YYYYMMDD'), '5551120934', 'soml@pmail.com');
-- Transactions table
INSERT INTO TRANSACTIONS VALUES (1, to_date('20141204','YYYYMMDD'), to_date('20141127','YYYYMMDD'), 1, to_date('20141129','YYYYMMDD'), 1, 1, 'Sue', 'Rabinski');
INSERT INTO TRANSACTIONS VALUES (2, to_date('20141205','YYYYMMDD'), to_date('20141129','YYYYMMDD'), 2, NULL, NULL, 1, 'Tom', 'Rabinski');
INSERT INTO TRANSACTIONS VALUES (3, to_date('20141207','YYYYMMDD'), to_date('20141204','YYYYMMDD'), 3, to_date('20141201','YYYYMMDD'), 1, 1, 'Dick', 'Rabinski');
INSERT INTO TRANSACTIONS VALUES (4, to_date('20141212','YYYYMMDD'), to_date('20141202','YYYYMMDD'), 4, to_date('20141202','YYYYMMDD'), 3, 2, 'Sally', 'Smith');
INSERT INTO TRANSACTIONS VALUES (5, to_date('20141202','YYYYMMDD'), to_date('20141125','YYYYMMDD'), 1, NULL, NULL, 4, 'Mary', 'Jarkowski');
INSERT INTO TRANSACTIONS VALUES (6, to_date('20141205','YYYYMMDD'), to_date('20141129','YYYYMMDD'), 2, NULL, NULL, 5, 'Beth', 'Badbreath');
INSERT INTO TRANSACTIONS VALUES (7, to_date('20141206','YYYYMMDD'), to_date('20141203','YYYYMMDD'), 2, to_date('20141203','YYYYMMDD'), 2, 6, 'Tina', 'Babe');
INSERT INTO TRANSACTIONS VALUES (8, to_date('20141210','YYYYMMDD'), to_date('20141204','YYYYMMDD'), 1, to_date('20141204','YYYYMMDD'), 1, 7, 'Doug', 'Seing');
INSERT INTO TRANSACTIONS VALUES (9, to_date('20141206','YYYYMMDD'), to_date('20141201','YYYYMMDD'), 1, to_date('20141202','YYYYMMDD'), 2, 8, 'Dave', 'Stimey');
INSERT INTO TRANSACTIONS VALUES (10, to_date('20141206','YYYYMMDD'), to_date('20141125','YYYYMMDD'), 1, NULL, NULL, 3, 'Dale', 'Eatheart');
INSERT INTO TRANSACTIONS VALUES (11, to_date('20141205','YYYYMMDD'), to_date('20141129','YYYYMMDD'), 2, NULL, NULL, 2, 'Sally', 'Smith');
INSERT INTO TRANSACTIONS VALUES (12, to_date('20141202','YYYYMMDD'), to_date('20141125','YYYYMMDD'), 1, to_date('20141202','YYYYMMDD'), 2, 2, 'Darin', 'Smith');
INSERT INTO TRANSACTIONS VALUES (13, to_date('20141205','YYYYMMDD'), to_date('20141129','YYYYMMDD'), 2, NULL, NULL, 8, 'Tim', 'Weski');
INSERT INTO TRANSACTIONS VALUES (14, to_date('20141202','YYYYMMDD'), to_date('20141125','YYYYMMDD'), 1, to_date('20141204','YYYYMMDD'), 1, 1, 'Dick', 'Rabinski');
INSERT INTO TRANSACTIONS VALUES (15, to_date('20141205','YYYYMMDD'), to_date('20141129','YYYYMMDD'), 2, NULL, NULL, 6, 'Tina', 'Babe');
-- VideoTrans Table
INSERT INTO VIDEOTRANS VALUES (1,1);
INSERT INTO VIDEOTRANS VALUES (1,3);
INSERT INTO VIDEOTRANS VALUES (1,5);
INSERT INTO VIDEOTRANS VALUES (2,19);
INSERT INTO VIDEOTRANS VALUES (3,17);
INSERT INTO VIDEOTRANS VALUES (4,4);
INSERT INTO VIDEOTRANS VALUES (4,2);
INSERT INTO VIDEOTRANS VALUES (5,10);
INSERT INTO VIDEOTRANS VALUES (6,11);
INSERT INTO VIDEOTRANS VALUES (7,11);
INSERT INTO VIDEOTRANS VALUES (8,14);
INSERT INTO VIDEOTRANS VALUES (8,13);
INSERT INTO VIDEOTRANS VALUES (8,19);
INSERT INTO VIDEOTRANS VALUES (9,9);
INSERT INTO VIDEOTRANS VALUES (10,8);
INSERT INTO VIDEOTRANS VALUES (11,15);
INSERT INTO VIDEOTRANS VALUES (12,15);
INSERT INTO VIDEOTRANS VALUES (12,11);
INSERT INTO VIDEOTRANS VALUES (12,10);
INSERT INTO VIDEOTRANS VALUES (13,4);
INSERT INTO VIDEOTRANS VALUES (14,6);
INSERT INTO VIDEOTRANS VALUES (15,7);

SET FEEDBACK ON
COMMIT

--
-- One query (per table) of the form SELECT * FROM table; in order to print 
    -- out your database

SELECT * FROM ACCOUNTS;
SELECT * FROM TRANSACTIONS;
SELECT * FROM VIDEOS;
SELECT * FROM STORESECTION;
SELECT * FROM STAFF; 
SELECT * FROM CUSTOMERS;
SELECT * FROM STOREDIN;
SELECT * FROM POSITIONS;
SELECT * FROM ZIPCODE;
SELECT * FROM VIDEOTRANS;
SELECT * FROM STAFFPOSITIONS;
--    

--
-- The SQL queries. Include the following for each query:
--1. A comment line stating the query number and feature(s) it demonstrates
--2. A comment line stating hte query in English.
--3. The SQL code for the query.
--
--
--1. A join involving at least four relations.
-- for promotional services:
-- find all the accounts that have  rented a video  
    -- rated G or PG
    -- print their First Name, Last Name, email, phone, and Address
SELECT DISTINCT C.F_Name, C.L_Name, C.Email, C.Phone, A.Address  
FROM ACCOUNTS A, TRANSACTIONS T, VIDEOTRANS VT, VIDEOS V, CUSTOMERS C
WHERE T.A_ID = A.A_ID AND 
    	A.A_ID = C.A_ID AND
    	T.Trans_Num = VT.TNum AND
    	VT.V_ID = V.V_ID AND
       	(V.Rating = 'G' OR
        V.Rating = 'PG');

--2. A self-join. (LEFT OUTER JOIN, INNER JOIN)
-- returns ids for customers with similar return dates
SELECT DISTINCT T1.L_Name, T2.L_Name
FROM TRANSACTIONS T1, TRANSACTIONS  T2
WHERE T1.Ret_Date = T2.Ret_Date AND
      T1.A_ID < T2.A_ID;

--3. UNION, INTERSECT, and/or MINUS.
-- Returns a set of customers who have joined in
-- the past five years and currently have a video
-- checked out.
SELECT C.A_ID, C.L_Name
FROM CUSTOMERS C
WHERE C.Date_joined > to_date('20090101','YYYYMMDD')
UNION
SELECT C.A_ID, C.L_Name
FROM TRANSACTIONS T, CUSTOMERS C
WHERE C.A_ID = T.A_ID AND
	T.Ret_Date IS NULL;

--4. SUM, AVG, MAX, and/or MIN
-- find the average number of videos rented in a transaction 
-- print  number
SELECT  AVG (count(V.V_ID))
FROM TRANSACTIONS T, VIDEOTRANS V
WHERE T.Trans_Num = V.TNum 
GROUP BY V.V_ID;

--5. GROUP BY , HAVING, and ORDER BY , all appearing in the same query
SELECT S.S_ID, S.Last_Name, COUNT(*)
FROM TRANSACTIONS T, STAFF S
WHERE S.S_ID = T.Staff_OUT
GROUP BY S.S_ID, S.Last_Name
HAVING COUNT(*) > 2
ORDER BY S.Last_Name;

--6. A correlated subquery.
    -- show all staff that have not rented a movie out
SELECT S.S_ID, S.Last_Name
FROM STAFF S
WHERE NOT EXISTS (SELECT * 
                    FROM TRANSACTIONS T
                    WHERE S.S_ID = T.Staff_OUT) 
ORDER BY S.S_ID;

--7. A non-correlated subquery.
    -- show all staff that have not rented a movie out
SELECT S.S_ID, S.Last_Name
FROM STAFF S
WHERE S.S_ID NOT IN (SELECT T.Staff_OUT 
                    FROM TRANSACTIONS T)
ORDER BY S.S_ID;

--8. A relational DIVISION query.
-- Shows Last name and Account ID for 
    -- accounts that have rented a western
SELECT DISTINCT C.L_Name, C.A_ID
FROM TRANSACTIONS C
WHERE NOT EXISTS ((SELECT G.Genre
		FROM VIDEOS G
		WHERE G.Genre = 'Western')
		MINUS
		(SELECT G.Genre
		FROM VIDEOTRANS T, VIDEOS G
		WHERE T.TNUM = C.Trans_Num AND
			T.V_ID = G.V_ID AND
			 G.Genre = 'Western'));

--9. An outer join query.
-- Display all of the video titles 
    -- their rating, genre, and the number of times they have been rented
SELECT V.TITLE, V.RATING, V.GENRE, count(T.TNum)  
FROM VIDEOS V LEFT OUTER JOIN VIDEOTRANS T ON V.V_ID = T.V_ID 
GROUP BY  V.TITLE, V.RATING, V.GENRE
ORDER BY V.TITLE;


--
-- The insert/delete/udpate statements to test teh enforcement of ICS
-- include the following fore EVERY IC that you test
    -- imporant: see the next section titled "Submit a final report" regarding
    -- which IC's to test
-- A comment line stating: Testing: <IC name>
-- A SQL INSERT/DELETE/UPDATE that will test the IC
--
-- Testing: aIC1
-- (Primary Key) Should break because it's attempting to insert a second account with the A_ID of '1'
INSERT INTO ACCOUNTS VALUES(1, '123 Sesame Street', 49401);
--
-- Testing: FOREIGN_KEY
-- (foreign key) Should break because it references an A_ID that doesn't exist.
INSERT INTO CUSTOMERS VALUES (9, to_date('19671228','YYYYMMDD'), 'Joe', 'Dirt', to_date('20131201','YYYYMMDD'), '5551234667', 'home1@home.com');
--
-- Testing: ssIC2
-- (1-attribute) Should break because 'videogames' isn't acceptable value of 'Section_Name'
INSERT INTO STORESECTION VALUES ('VideoGames', 25, 8);
--
-- Testing: tIC2
-- (2-attribute, 1-row) Should break because 'ret_date' = NULL but 'staff_in' is NOT NULL
INSERT INTO TRANSACTIONS VALUES (1, to_date('20141204','YYYYMMDD'), to_date('20141127','YYYYMMDD'), 1, NULL, 1, 1, 'Sue', 'Rabinski');
--

-- Notes specific to this database. 
-- I believe as of 11:20 on 12/3 I have all of the constraints accounted for that we need. 
-- We have our weak key with the Account/customer setup
-- Transaction is the backbone and is setup that way. 
-- Note that to tell if a video is out check the ReturnDate and Staff In fields.
--     They should be null if the video is still out.
-- The customer key is a 3 part key. It has the account # and the first and last names.
--    This is our weak key. 
-- Age is calculated from the date of birth
-- Due dates for rentals can use the same idea.
--
-- UNLESS MAJOR ERRORS WERE MADE PLEASE DO NOT CHANGE DATABASE STRUCTURE 
--
-- TODO  Verify the ER document now accurately reflects the database as is.
-- We should NOT have to add any more fields or tables.
-- TODO verify the specifications and schema also match the database because there have
--  been some changes to make everything work. 
-- TODO form way to calculate age and due dates from a date sent to the query. Note 
--     the format you have to use to enter the date

-- COMMIT
SPOOL OFF
